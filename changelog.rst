Changelog
=========


0.3.58 (2024-06-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.57' into 'main' [Marvin van Aalst]

  0.3.57

  See merge request marvin.vanaalst/qtb-plot!71
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.57 (2024-06-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.56' into 'main' [Marvin van Aalst]

  0.3.56

  See merge request marvin.vanaalst/qtb-plot!70
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.56 (2024-06-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.55' into 'main' [Marvin van Aalst]

  0.3.55

  See merge request marvin.vanaalst/qtb-plot!69
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.55 (2024-05-27)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.54' into 'main' [Marvin van Aalst]

  0.3.54

  See merge request marvin.vanaalst/qtb-plot!68
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.54 (2024-05-20)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.53' into 'main' [Marvin van Aalst]

  0.3.53

  See merge request marvin.vanaalst/qtb-plot!67
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.53 (2024-05-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.52' into 'main' [Marvin van Aalst]

  0.3.52

  See merge request marvin.vanaalst/qtb-plot!66
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.52 (2024-05-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.51' into 'main' [Marvin van Aalst]

  0.3.51

  See merge request marvin.vanaalst/qtb-plot!65
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.51 (2024-04-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.50' into 'main' [Marvin van Aalst]

  0.3.50

  See merge request marvin.vanaalst/qtb-plot!64
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.50 (2024-03-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.49' into 'main' [Marvin van Aalst]

  0.3.49

  See merge request marvin.vanaalst/qtb-plot!63
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.49 (2024-03-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.48' into 'main' [Marvin van Aalst]

  0.3.48

  See merge request marvin.vanaalst/qtb-plot!62
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.48 (2024-03-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.47' into 'main' [Marvin van Aalst]

  0.3.47

  See merge request marvin.vanaalst/qtb-plot!61
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.47 (2024-02-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.46' into 'main' [Marvin van Aalst]

  0.3.46

  See merge request marvin.vanaalst/qtb-plot!60
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.46 (2024-02-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.45' into 'main' [Marvin van Aalst]

  0.3.45

  See merge request marvin.vanaalst/qtb-plot!59
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.45 (2024-02-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.44' into 'main' [Marvin van Aalst]

  0.3.44

  See merge request marvin.vanaalst/qtb-plot!58
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.44 (2024-02-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.43' into 'main' [Marvin van Aalst]

  0.3.43

  See merge request marvin.vanaalst/qtb-plot!57
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.43 (2024-01-29)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.42' into 'main' [Marvin van Aalst]

  0.3.42

  See merge request marvin.vanaalst/qtb-plot!56
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.42 (2024-01-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.41' into 'main' [Marvin van Aalst]

  0.3.41

  See merge request marvin.vanaalst/qtb-plot!55
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.41 (2024-01-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.40' into 'main' [Marvin van Aalst]

  0.3.40

  See merge request marvin.vanaalst/qtb-plot!54
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.40 (2024-01-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.39' into 'main' [Marvin van Aalst]

  0.3.39

  See merge request marvin.vanaalst/qtb-plot!53
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.39 (2023-12-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.38' into 'main' [Marvin van Aalst]

  0.3.38

  See merge request marvin.vanaalst/qtb-plot!52
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.38 (2023-12-20)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Dev: chg: unified gitlab ci. [Marvin van Aalst]
- Dev: fix: set notebook version < 7.0. [Marvin van Aalst]
- Merge branch 'v0.3.37' into 'main' [Marvin van Aalst]

  0.3.37

  See merge request marvin.vanaalst/qtb-plot!51
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.37 (2023-12-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.3.36' into 'main' [Marvin van Aalst]

  0.3.36

  See merge request marvin.vanaalst/qtb-plot!50
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.36 (2023-11-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: pre-commit. [Marvin van Aalst]
- Dev: chg: black. [Marvin van Aalst]
- Dev: chg: unified black settings. [Marvin van Aalst]
- Merge branch 'v0.3.35' into 'main' [Marvin van Aalst]

  0.3.35

  See merge request marvin.vanaalst/qtb-plot!49
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.35 (2023-11-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: included bandit security checks. [Marvin van Aalst]
- Dev: chg: included bandit security checks. [Marvin van Aalst]
- Dev: chg: included bandit security checks. [Marvin van Aalst]
- Merge branch 'v0.3.34' into 'main' [Marvin van Aalst]

  0.3.34

  See merge request marvin.vanaalst/qtb-plot!48
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.34 (2023-11-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.33' into 'main' [Marvin van Aalst]

  0.3.33

  See merge request marvin.vanaalst/qtb-plot!47
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.33 (2023-10-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.32' into 'main' [Marvin van Aalst]

  0.3.32

  See merge request marvin.vanaalst/qtb-plot!46
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.32 (2023-10-16)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.31' into 'main' [Marvin van Aalst]

  0.3.31

  See merge request marvin.vanaalst/qtb-plot!45
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.31 (2023-10-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.30' into 'main' [Marvin van Aalst]

  0.3.30

  See merge request marvin.vanaalst/qtb-plot!44
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.30 (2023-10-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.29' into 'main' [Marvin van Aalst]

  0.3.29

  See merge request marvin.vanaalst/qtb-plot!43
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.29 (2023-09-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.28' into 'main' [Marvin van Aalst]

  0.3.28

  See merge request marvin.vanaalst/qtb-plot!42
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.28 (2023-09-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.27' into 'main' [Marvin van Aalst]

  0.3.27

  See merge request marvin.vanaalst/qtb-plot!41
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.27 (2023-09-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.26' into 'main' [Marvin van Aalst]

  0.3.26

  See merge request marvin.vanaalst/qtb-plot!40
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.26 (2023-09-04)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.25' into 'main' [Marvin van Aalst]

  0.3.25

  See merge request marvin.vanaalst/qtb-plot!39
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.25 (2023-08-28)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.24' into 'main' [Marvin van Aalst]

  0.3.24

  See merge request marvin.vanaalst/qtb-plot!38
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.24 (2023-08-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.23' into 'main' [Marvin van Aalst]

  0.3.23

  See merge request marvin.vanaalst/qtb-plot!37
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.23 (2023-08-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.22' into 'main' [Marvin van Aalst]

  0.3.22

  See merge request marvin.vanaalst/qtb-plot!36
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.22 (2023-08-07)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.21' into 'main' [Marvin van Aalst]

  0.3.21

  See merge request marvin.vanaalst/qtb-plot!35
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.21 (2023-07-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.20' into 'main' [Marvin van Aalst]

  0.3.20

  See merge request marvin.vanaalst/qtb-plot!34
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.20 (2023-07-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.19' into 'main' [Marvin van Aalst]

  0.3.19

  See merge request marvin.vanaalst/qtb-plot!33
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.19 (2023-07-17)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.18' into 'main' [Marvin van Aalst]

  0.3.18

  See merge request marvin.vanaalst/qtb-plot!32
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.18 (2023-07-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.17' into 'main' [Marvin van Aalst]

  0.3.17

  See merge request marvin.vanaalst/qtb-plot!31
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.17 (2023-07-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.16' into 'main' [Marvin van Aalst]

  0.3.16

  See merge request marvin.vanaalst/qtb-plot!30
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.16 (2023-06-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.15' into 'main' [Marvin van Aalst]

  0.3.15

  See merge request marvin.vanaalst/qtb-plot!29
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.15 (2023-06-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.14' into 'main' [Marvin van Aalst]

  0.3.14

  See merge request marvin.vanaalst/qtb-plot!28
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.14 (2023-06-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.13' into 'main' [Marvin van Aalst]

  0.3.13

  See merge request marvin.vanaalst/qtb-plot!27
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.13 (2023-06-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.12' into 'main' [Marvin van Aalst]

  0.3.12

  See merge request marvin.vanaalst/qtb-plot!26
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.12 (2023-05-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.11' into 'main' [Marvin van Aalst]

  0.3.11

  See merge request marvin.vanaalst/qtb-plot!25
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.11 (2023-05-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.10' into 'main' [Marvin van Aalst]

  0.3.10

  See merge request marvin.vanaalst/qtb-plot!24
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.10 (2023-05-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.9' into 'main' [Marvin van Aalst]

  0.3.9

  See merge request marvin.vanaalst/qtb-plot!23
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.9 (2023-04-11)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.8' into 'main' [Marvin van Aalst]

  0.3.8

  See merge request marvin.vanaalst/qtb-plot!22
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.8 (2023-03-31)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.7' into 'main' [Marvin van Aalst]

  0.3.7

  See merge request marvin.vanaalst/qtb-plot!21
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.7 (2023-03-13)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.6' into 'main' [Marvin van Aalst]

  0.3.6

  See merge request marvin.vanaalst/qtb-plot!20
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.6 (2023-03-06)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.5' into 'main' [Marvin van Aalst]

  0.3.5

  See merge request marvin.vanaalst/qtb-plot!19
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.5 (2023-02-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.4' into 'main' [Marvin van Aalst]

  0.3.4

  See merge request marvin.vanaalst/qtb-plot!18
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.4 (2023-02-06)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.3' into 'main' [Marvin van Aalst]

  0.3.3

  See merge request marvin.vanaalst/qtb-plot!17
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.3 (2023-01-30)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.2' into 'main' [Marvin van Aalst]

  0.3.2

  See merge request marvin.vanaalst/qtb-plot!16
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.2 (2023-01-16)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.3.1' into 'main' [Marvin van Aalst]

  0.3.1

  See merge request marvin.vanaalst/qtb-plot!15
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.1 (2023-01-09)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: manual update. [Marvin van Aalst]
- Merge branch 'v0.2.13' into 'main' [Marvin van Aalst]

  0.2.13

  See merge request marvin.vanaalst/qtb-plot!14
- Bot: chg: updated changelog. [Marvin van Aalst]


0.4.0 (2023-01-02)
------------------
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.13 (2023-01-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]


0.3.0 (2022-12-27)
------------------
- Merge branch 'v0.2.12' into 'main' [Marvin van Aalst]

  0.2.12

  See merge request marvin.vanaalst/qtb-plot!13
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.12 (2022-12-27)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.11' into 'main' [Marvin van Aalst]

  0.2.11

  See merge request marvin.vanaalst/qtb-plot!12
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.11 (2022-12-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.10' into 'main' [Marvin van Aalst]

  0.2.10

  See merge request marvin.vanaalst/qtb-plot!11
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.10 (2022-12-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.2.9' into 'main' [Marvin van Aalst]

  0.2.9

  See merge request marvin.vanaalst/qtb-plot!10
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.9 (2022-11-29)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.8

  See merge request marvin.vanaalst/qtb-plot!9
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.8 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated python version bound. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.7

  See merge request marvin.vanaalst/qtb-plot!8
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.7 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.6

  See merge request marvin.vanaalst/qtb-plot!7
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.6 (2022-11-14)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.5

  See merge request marvin.vanaalst/qtb-plot!6
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.5 (2022-11-07)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.4

  See merge request marvin.vanaalst/qtb-plot!5
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.4 (2022-10-31)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.3

  See merge request marvin.vanaalst/qtb-plot!4
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.3 (2022-10-26)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated ci container. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.2

  See merge request marvin.vanaalst/qtb-plot!3
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.2 (2022-10-24)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.2.1

  See merge request marvin.vanaalst/qtb-plot!2
- Dev: fix: test. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.1 (2022-09-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: added changelog to dev dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  dev: chg: added dev dependencies

  See merge request marvin.vanaalst/qtb-plot!1
- Dev: chg: changed gitlab url. [Marvin van Aalst]
- Dev: chg: added dev dependencies. [Marvin van Aalst]
- Dev: new: proper project layout. [Marvin van Aalst]
- Dev: new: added .gitattributes. [Marvin van Aalst]
- Updated .gitignore. [Marvin van Aalst]
- Merge branch 'master' of gitlab.com:marvin.vanaalst/qtb_plot. [Marvin
  van Aalst]
- Update README.md. [Marvin van Aalst]
- Added some more color schemes. [Marvin van Aalst]
- Set notebook to the default mode. [Marvin van Aalst]
- Updated the package. [Marvin van Aalst]
- Fixed standard colormap. [Marvin van Aalst]
- Added docstring. [Marvin van Aalst]
- Initial commit. [Marvin van Aalst]


